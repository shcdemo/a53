<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="A53687">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>The oath of every free-man of the city of London</title>
    <author>City of London (England).</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription A53687 of text R221935 in the <ref target="http;//estc.bl.uk">English Short Title Catalog</ref> (Wing O73B). Textual changes  and metadata enrichments aim at making the                             text more  computationally tractable, easier to read, and suitable for network-based collaborative                              curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in                             a standardized format that preserves archaic forms ('loveth', 'seekest').                              Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 3 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2018</date>
    <idno type="DLPS">A53687</idno>
    <idno type="STC">Wing O73B</idno>
    <idno type="STC">ESTC R221935</idno>
    <idno type="EEBO-CITATION">99833179</idno>
    <idno type="PROQUEST">99833179</idno>
    <idno type="VID">37654</idno>
    <idno type="PROQUESTGOID">2240941304</idno>
    <availability>
     <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 1, no. A53687)</note>
    <note>Transcribed from: (Early English Books Online ; image set 37654)</note>
    <note>Images scanned from microfilm: (Early English books, 1641-1700 ; 2192:11)</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>The oath of every free-man of the city of London</title>
      <author>City of London (England).</author>
     </titleStmt>
     <extent>2 sheets ([2] p.)</extent>
     <publicationStmt>
      <publisher>Printed by James Flesher, printer to this honourable city,</publisher>
      <pubPlace>[London] :</pubPlace>
      <date>[1653?]</date>
     </publicationStmt>
     <notesStmt>
      <note>Date of publication from Wing.</note>
      <note>Title of second sheet: Instructions for every free-man of the city of London.</note>
      <note>"You shall sweare, that ye shall be true and faithfull to the Common-wealth of England; and in"--first three lines of text.</note>
      <note>Copy torn with loss of text.</note>
      <note>Reproduction of the original in the Harvard University Library.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>Loyalty oaths -- England -- Early works to 1800.</term>
     <term>Great Britain -- History -- Commonwealth and Protectorate, 1642-1660 -- Early works to 1800.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:corpus>eebo</ep:corpus>
    <ep:title>The oath of every free-man of the city of London.</ep:title>
    <ep:author>Corporation of London</ep:author>
    <ep:publicationYear>1653</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>2</ep:pageCount>
    <ep:wordCount>478</ep:wordCount>
    <ep:defectiveTokenCount>2</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>41.84</ep:defectRate>
    <ep:finalGrade>D</ep:finalGrade>
    <ep:defectRangePerGrade> The  rate of 41.84 defects per 10,000 words puts this text in the D category of texts with between 35 and 100 defects per 10,000 words.</ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change><date>2008-04</date><label>TCP</label>
        Assigned for keying and markup
      </change>
   <change><date>2008-08</date><label>SPi Global</label>
        Keyed and coded from ProQuest page images
      </change>
   <change><date>2008-09</date><label>Megan Marion</label>
        Sampled and proofread
      </change>
   <change><date>2008-09</date><label>Megan Marion</label>
        Text and markup reviewed and edited
      </change>
   <change><date>2009-02</date><label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="A53687-t">
  <group xml:id="A53687-e0">
   <text xml:id="A53687-e10">
    <body xml:id="A53687-e20">
     <div type="document" xml:id="A53687-e30">
      <pb facs="tcp:37654:1" xml:id="A53687-001-a"/>
      <head xml:id="A53687-e40">
       <figure xml:id="A53687-e50">
        <figDesc xml:id="A53687-e60">coat of arms of the City of London</figDesc>
       </figure>
       <w lemma="the" pos="d" xml:id="A53687-001-a-0010">The</w>
       <w lemma="oath" pos="n1" xml:id="A53687-001-a-0020">Oath</w>
       <w lemma="of" pos="acp" xml:id="A53687-001-a-0030">of</w>
       <w lemma="every" pos="d" xml:id="A53687-001-a-0040">every</w>
       <w lemma="freeman" pos="n1" reg="Freeman" xml:id="A53687-001-a-0050">Free-man</w>
       <w lemma="of" pos="acp" xml:id="A53687-001-a-0060">of</w>
       <w lemma="the" pos="d" xml:id="A53687-001-a-0070">the</w>
       <w lemma="city" pos="n1" xml:id="A53687-001-a-0080">City</w>
       <w lemma="of" pos="acp" xml:id="A53687-001-a-0090">of</w>
       <w lemma="London" pos="nn1" rend="hi" xml:id="A53687-001-a-0100">London</w>
       <pc unit="sentence" xml:id="A53687-001-a-0110">.</pc>
      </head>
      <p xml:id="A53687-e80">
       <w lemma="you" pos="pn" reg="YOU" rend="decorinit" xml:id="A53687-001-a-0120">YOV</w>
       <w lemma="shall" pos="vmb" xml:id="A53687-001-a-0130">shall</w>
       <w lemma="swear" pos="vvi" reg="Swear" xml:id="A53687-001-a-0140">Sweare</w>
       <pc xml:id="A53687-001-a-0150">,</pc>
       <w lemma="that" pos="cs" xml:id="A53687-001-a-0160">that</w>
       <w lemma="you" pos="pn" xml:id="A53687-001-a-0170">you</w>
       <w lemma="shall" pos="vmb" xml:id="A53687-001-a-0180">shall</w>
       <w lemma="be" pos="vvi" xml:id="A53687-001-a-0190">be</w>
       <w lemma="true" pos="j" xml:id="A53687-001-a-0200">true</w>
       <w lemma="and" pos="cc" xml:id="A53687-001-a-0210">and</w>
       <w lemma="faithful" pos="j" reg="faithful" xml:id="A53687-001-a-0220">faithfull</w>
       <w lemma="to" pos="prt" xml:id="A53687-001-a-0230">to</w>
       <w lemma="the" pos="d" xml:id="A53687-001-a-0240">the</w>
       <w lemma="commonwealth" pos="n1" reg="Commonwealth" xml:id="A53687-001-a-0250">Common-wealth</w>
       <w lemma="of" pos="acp" xml:id="A53687-001-a-0260">of</w>
       <w lemma="England" pos="nn1" rend="hi" xml:id="A53687-001-a-0270">England</w>
       <pc xml:id="A53687-001-a-0280">;</pc>
       <w lemma="and" pos="cc" xml:id="A53687-001-a-0290">and</w>
       <w lemma="in" pos="acp" xml:id="A53687-001-a-0300">in</w>
       <w lemma="order" pos="n1" xml:id="A53687-001-a-0310">order</w>
       <w lemma="thereunto" pos="av" xml:id="A53687-001-a-0320">thereunto</w>
       <pc xml:id="A53687-001-a-0330">,</pc>
       <w lemma="you" pos="pn" xml:id="A53687-001-a-0340">You</w>
       <w lemma="shall" pos="vmb" xml:id="A53687-001-a-0350">shall</w>
       <w lemma="be" pos="vvi" xml:id="A53687-001-a-0360">be</w>
       <w lemma="obedient" pos="j" xml:id="A53687-001-a-0370">obedient</w>
       <w lemma="to" pos="acp" xml:id="A53687-001-a-0380">to</w>
       <w lemma="the" pos="d" xml:id="A53687-001-a-0390">the</w>
       <w lemma="just" pos="j" reg="just" xml:id="A53687-001-a-0400">iust</w>
       <w lemma="and" pos="cc" xml:id="A53687-001-a-0410">and</w>
       <w lemma="good" pos="j" xml:id="A53687-001-a-0420">good</w>
       <w lemma="government" pos="n1" xml:id="A53687-001-a-0430">Government</w>
       <w lemma="of" pos="acp" xml:id="A53687-001-a-0440">of</w>
       <w lemma="this" pos="d" xml:id="A53687-001-a-0450">this</w>
       <w lemma="city" pos="n1" xml:id="A53687-001-a-0460">City</w>
       <w lemma="of" pos="acp" xml:id="A53687-001-a-0470">of</w>
       <w lemma="London" pos="nn1" rend="hi" xml:id="A53687-001-a-0480">London</w>
       <pc xml:id="A53687-001-a-0490">:</pc>
       <w lemma="you" pos="pn" xml:id="A53687-001-a-0500">You</w>
       <w lemma="shall" pos="vmb" xml:id="A53687-001-a-0510">shall</w>
       <w lemma="to" pos="acp" xml:id="A53687-001-a-0520">to</w>
       <w lemma="the" pos="d" xml:id="A53687-001-a-0530">the</w>
       <w lemma="best" pos="js" xml:id="A53687-001-a-0540">best</w>
       <w lemma="of" pos="acp" xml:id="A53687-001-a-0550">of</w>
       <w lemma="your" pos="po" xml:id="A53687-001-a-0560">your</w>
       <w lemma="power" pos="n1" xml:id="A53687-001-a-0570">power</w>
       <w lemma="maintain" pos="vvi" xml:id="A53687-001-a-0580">maintain</w>
       <w lemma="and" pos="cc" xml:id="A53687-001-a-0590">and</w>
       <w lemma="preserve" pos="vvi" xml:id="A53687-001-a-0600">preserve</w>
       <w lemma="the" pos="d" xml:id="A53687-001-a-0610">the</w>
       <w lemma="peace" pos="n1" xml:id="A53687-001-a-0620">Peace</w>
       <pc xml:id="A53687-001-a-0630">,</pc>
       <w lemma="and" pos="cc" xml:id="A53687-001-a-0640">and</w>
       <w lemma="all" pos="d" xml:id="A53687-001-a-0650">all</w>
       <w lemma="the" pos="d" xml:id="A53687-001-a-0660">the</w>
       <w lemma="due" pos="j" xml:id="A53687-001-a-0670">due</w>
       <w lemma="franchise" pos="n2" xml:id="A53687-001-a-0680">Franchises</w>
       <w lemma="thereof" pos="av" xml:id="A53687-001-a-0690">thereof</w>
       <pc xml:id="A53687-001-a-0700">;</pc>
       <w lemma="and" pos="cc" xml:id="A53687-001-a-0710">and</w>
       <w lemma="according" pos="j" xml:id="A53687-001-a-0720">according</w>
       <w lemma="to" pos="acp" xml:id="A53687-001-a-0730">to</w>
       <w lemma="your" pos="po" xml:id="A53687-001-a-0740">your</w>
       <w lemma="knowledge" pos="n1" reg="knowledge" xml:id="A53687-001-a-0750">knowledg</w>
       <w lemma="and" pos="cc" xml:id="A53687-001-a-0760">and</w>
       <w lemma="ability" pos="n1" xml:id="A53687-001-a-0770">ability</w>
       <pc xml:id="A53687-001-a-0780">,</pc>
       <w lemma="do" pos="vvb" reg="do" xml:id="A53687-001-a-0790">doe</w>
       <w lemma="and" pos="cc" xml:id="A53687-001-a-0800">and</w>
       <w lemma="perform" pos="vvi" xml:id="A53687-001-a-0810">perform</w>
       <w lemma="all" pos="d" xml:id="A53687-001-a-0820">all</w>
       <w lemma="such" pos="d" xml:id="A53687-001-a-0830">such</w>
       <w lemma="other" pos="d" xml:id="A53687-001-a-0840">other</w>
       <w lemma="act" pos="n2" xml:id="A53687-001-a-0850">acts</w>
       <w lemma="and" pos="cc" xml:id="A53687-001-a-0860">and</w>
       <w lemma="〈◊〉" pos="zz" xml:id="A53687-001-a-0870">〈◊〉</w>
       <w lemma="be" pos="vvz" xml:id="A53687-001-a-0880">s</w>
       <w lemma="as" pos="acp" xml:id="A53687-001-a-0890">as</w>
       <w lemma="do" pos="vvi" reg="do" xml:id="A53687-001-a-0900">doe</w>
       <w lemma="belong" pos="vvi" xml:id="A53687-001-a-0910">belong</w>
       <w lemma="to" pos="acp" xml:id="A53687-001-a-0920">to</w>
       <w lemma="a" pos="d" xml:id="A53687-001-a-0930">a</w>
       <w lemma="freeman" pos="n1" reg="Freeman" xml:id="A53687-001-a-0940">Free-man</w>
       <w lemma="of" pos="acp" xml:id="A53687-001-a-0950">of</w>
       <w lemma="〈◊〉" pos="zz" xml:id="A53687-001-a-0960">〈◊〉</w>
       <w lemma="ud" pos="j" xml:id="A53687-001-a-0970">ud</w>
       <w lemma="city" pos="n1" xml:id="A53687-001-a-0980">City</w>
       <pc unit="sentence" xml:id="A53687-001-a-0990">.</pc>
      </p>
     </div>
    </body>
    <back xml:id="A53687-e110">
     <div type="colophon" xml:id="A53687-e120">
      <p xml:id="A53687-e130">
       <w lemma="print" pos="j-vn" xml:id="A53687-001-a-1000">Printed</w>
       <w lemma="by" pos="acp" xml:id="A53687-001-a-1010">by</w>
       <hi xml:id="A53687-e140">
        <w lemma="James" pos="nn1" xml:id="A53687-001-a-1020">James</w>
        <w lemma="flesher" pos="nn1" xml:id="A53687-001-a-1030">Flesher</w>
       </hi>
       <pc rend="follows-hi" xml:id="A53687-001-a-1040">,</pc>
       <w lemma="printer" pos="n1" xml:id="A53687-001-a-1050">Printer</w>
       <w lemma="to" pos="acp" xml:id="A53687-001-a-1060">to</w>
       <w lemma="this" pos="d" xml:id="A53687-001-a-1070">this</w>
       <w lemma="honourable" pos="j" xml:id="A53687-001-a-1080">Honourable</w>
       <w lemma="city" pos="n1" xml:id="A53687-001-a-1090">City</w>
       <pc unit="sentence" xml:id="A53687-001-a-1100">.</pc>
      </p>
     </div>
    </back>
   </text>
   <text xml:id="A53687-e150">
    <body xml:id="A53687-e160">
     <div type="document" xml:id="A53687-e170">
      <pb facs="tcp:37654:1" xml:id="A53687-001-b"/>
      <head xml:id="A53687-e180">
       <figure xml:id="A53687-e190">
        <figDesc xml:id="A53687-e200">coat of arms of the City of London</figDesc>
       </figure>
       <w lemma="instruction" pos="n2" xml:id="A53687-001-b-0010">Instructions</w>
       <w lemma="for" pos="acp" xml:id="A53687-001-b-0020">for</w>
       <w lemma="every" pos="d" xml:id="A53687-001-b-0030">every</w>
       <w lemma="freeman" pos="n1" reg="Freeman" xml:id="A53687-001-b-0040">Free-man</w>
       <w lemma="of" pos="acp" xml:id="A53687-001-b-0050">of</w>
       <w lemma="the" pos="d" xml:id="A53687-001-b-0060">the</w>
       <w lemma="city" pos="n1" xml:id="A53687-001-b-0070">City</w>
       <w lemma="of" pos="acp" xml:id="A53687-001-b-0080">of</w>
       <w lemma="LONDON" pos="nn1" xml:id="A53687-001-b-0090">LONDON</w>
       <pc unit="sentence" xml:id="A53687-001-b-0100">.</pc>
      </head>
      <p xml:id="A53687-e210">
       <w lemma="you" pos="pn" rend="decorinit" xml:id="A53687-001-b-0110">YE</w>
       <w lemma="shall" pos="vmb" xml:id="A53687-001-b-0120">shall</w>
       <w lemma="be" pos="vvi" xml:id="A53687-001-b-0130">be</w>
       <w lemma="obedient" pos="j" xml:id="A53687-001-b-0140">obedient</w>
       <w lemma="to" pos="acp" xml:id="A53687-001-b-0150">to</w>
       <w lemma="the" pos="d" xml:id="A53687-001-b-0160">the</w>
       <w lemma="mayor" pos="n1" reg="Mayor" xml:id="A53687-001-b-0170">Maior</w>
       <w lemma="and" pos="cc" xml:id="A53687-001-b-0180">and</w>
       <w lemma="minister" pos="n2" xml:id="A53687-001-b-0190">Ministers</w>
       <w lemma="of" pos="acp" xml:id="A53687-001-b-0200">of</w>
       <w lemma="this" pos="d" xml:id="A53687-001-b-0210">this</w>
       <w lemma="city" pos="n1" xml:id="A53687-001-b-0220">City</w>
       <pc unit="sentence" xml:id="A53687-001-b-0230">.</pc>
       <w lemma="the" pos="d" xml:id="A53687-001-b-0240">The</w>
       <w lemma="franchise" pos="n2" xml:id="A53687-001-b-0250">Franchises</w>
       <w lemma="and" pos="cc" xml:id="A53687-001-b-0260">and</w>
       <w lemma="custom" pos="n2" reg="Customs" xml:id="A53687-001-b-0270">Customes</w>
       <w lemma="thereof" pos="av" xml:id="A53687-001-b-0280">thereof</w>
       <w lemma="you" pos="pn" xml:id="A53687-001-b-0290">ye</w>
       <w lemma="shall" pos="vmb" xml:id="A53687-001-b-0300">shall</w>
       <w lemma="mantain" pos="n1" xml:id="A53687-001-b-0310">mantain</w>
       <pc xml:id="A53687-001-b-0320">,</pc>
       <w lemma="and" pos="cc" xml:id="A53687-001-b-0330">and</w>
       <w lemma="this" pos="d" xml:id="A53687-001-b-0340">this</w>
       <w lemma="city" pos="n1" xml:id="A53687-001-b-0350">City</w>
       <w lemma="keep" pos="vvi" xml:id="A53687-001-b-0360">keep</w>
       <w lemma="harmless" pos="j" reg="harmless" xml:id="A53687-001-b-0370">harmlesse</w>
       <w lemma="in" pos="acp" xml:id="A53687-001-b-0380">in</w>
       <w lemma="that" pos="cs" xml:id="A53687-001-b-0390">that</w>
       <w lemma="that" pos="cs" xml:id="A53687-001-b-0400">that</w>
       <w lemma="in" pos="acp" xml:id="A53687-001-b-0410">in</w>
       <w lemma="you" pos="pn" xml:id="A53687-001-b-0420">you</w>
       <w lemma="be" pos="vvz" xml:id="A53687-001-b-0430">is</w>
       <pc unit="sentence" xml:id="A53687-001-b-0440">.</pc>
       <w lemma="you" pos="pn" xml:id="A53687-001-b-0450">Ye</w>
       <w lemma="shall" pos="vmb" xml:id="A53687-001-b-0460">shall</w>
       <w lemma="be" pos="vvi" xml:id="A53687-001-b-0470">be</w>
       <w lemma="contributory" pos="j" reg="contributory" xml:id="A53687-001-b-0480">contributary</w>
       <w lemma="to" pos="acp" xml:id="A53687-001-b-0490">to</w>
       <w lemma="all" pos="d" xml:id="A53687-001-b-0500">all</w>
       <w lemma="manner" pos="n1" xml:id="A53687-001-b-0510">manner</w>
       <w lemma="of" pos="acp" xml:id="A53687-001-b-0520">of</w>
       <w lemma="charge" pos="n2" xml:id="A53687-001-b-0530">charges</w>
       <w lemma="within" pos="acp" xml:id="A53687-001-b-0540">within</w>
       <w lemma="this" pos="d" xml:id="A53687-001-b-0550">this</w>
       <w lemma="city" pos="n1" xml:id="A53687-001-b-0560">City</w>
       <pc xml:id="A53687-001-b-0570">,</pc>
       <w lemma="as" pos="acp" xml:id="A53687-001-b-0580">as</w>
       <w lemma="summons" pos="n1" xml:id="A53687-001-b-0590">Summons</w>
       <pc xml:id="A53687-001-b-0600">,</pc>
       <w lemma="watch" pos="n2" xml:id="A53687-001-b-0610">Watches</w>
       <pc xml:id="A53687-001-b-0620">,</pc>
       <w lemma="contribution" pos="n2" xml:id="A53687-001-b-0630">Contributions</w>
       <pc xml:id="A53687-001-b-0640">,</pc>
       <w lemma="tare" pos="n2" xml:id="A53687-001-b-0650">Tares</w>
       <pc xml:id="A53687-001-b-0660">,</pc>
       <w lemma="tallage" pos="n2" xml:id="A53687-001-b-0670">Tallages</w>
       <pc xml:id="A53687-001-b-0680">,</pc>
       <w lemma="lot" pos="n1" xml:id="A53687-001-b-0690">Lot</w>
       <pc xml:id="A53687-001-b-0700">,</pc>
       <w lemma="and" pos="cc" xml:id="A53687-001-b-0710">and</w>
       <w lemma="scot" pos="n1" xml:id="A53687-001-b-0720">Scot</w>
       <pc xml:id="A53687-001-b-0730">,</pc>
       <w lemma="and" pos="cc" xml:id="A53687-001-b-0740">and</w>
       <w lemma="to" pos="acp" xml:id="A53687-001-b-0750">to</w>
       <w lemma="all" pos="d" xml:id="A53687-001-b-0760">all</w>
       <w lemma="other" pos="d" xml:id="A53687-001-b-0770">other</w>
       <w lemma="charge" pos="n2" xml:id="A53687-001-b-0780">charges</w>
       <pc xml:id="A53687-001-b-0790">,</pc>
       <w lemma="bear" pos="vvg" xml:id="A53687-001-b-0800">bearing</w>
       <w lemma="your" pos="po" xml:id="A53687-001-b-0810">your</w>
       <w lemma="part" pos="n1" xml:id="A53687-001-b-0820">part</w>
       <w lemma="as" pos="acp" xml:id="A53687-001-b-0830">as</w>
       <w lemma="a" pos="d" xml:id="A53687-001-b-0840">a</w>
       <w lemma="freeman" pos="n1" reg="Freeman" xml:id="A53687-001-b-0850">Free-man</w>
       <w lemma="ought" pos="vmd" xml:id="A53687-001-b-0860">ought</w>
       <w lemma="to" pos="prt" xml:id="A53687-001-b-0870">to</w>
       <w lemma="do" pos="vvi" reg="do" xml:id="A53687-001-b-0880">doe</w>
       <pc unit="sentence" xml:id="A53687-001-b-0890">.</pc>
       <w lemma="you" pos="pn" xml:id="A53687-001-b-0900">Ye</w>
       <w lemma="shall" pos="vmb" xml:id="A53687-001-b-0910">shall</w>
       <w lemma="colour" pos="vvi" xml:id="A53687-001-b-0920">colour</w>
       <w lemma="no" pos="dx" xml:id="A53687-001-b-0930">no</w>
       <w lemma="foreign" pos="j" reg="Foreign" xml:id="A53687-001-b-0940">Forreign</w>
       <w lemma="good" pos="n2-j" xml:id="A53687-001-b-0950">goods</w>
       <pc xml:id="A53687-001-b-0960">,</pc>
       <w lemma="under" pos="acp" xml:id="A53687-001-b-0970">under</w>
       <pc xml:id="A53687-001-b-0980">,</pc>
       <w lemma="or" pos="cc" xml:id="A53687-001-b-0990">or</w>
       <w lemma="in" pos="acp" xml:id="A53687-001-b-1000">in</w>
       <w lemma="your" pos="po" xml:id="A53687-001-b-1010">your</w>
       <w lemma="name" pos="n1" xml:id="A53687-001-b-1020">name</w>
       <pc xml:id="A53687-001-b-1030">,</pc>
       <w lemma="whereby" pos="crq" xml:id="A53687-001-b-1040">whereby</w>
       <w lemma="his" pos="po" xml:id="A53687-001-b-1050">his</w>
       <w lemma="highness" pos="n1" reg="Highness" xml:id="A53687-001-b-1060">Highnesse</w>
       <w lemma="the" pos="d" xml:id="A53687-001-b-1070">the</w>
       <w lemma="lord" pos="n1" xml:id="A53687-001-b-1080">Lord</w>
       <w lemma="protector" pos="n1" xml:id="A53687-001-b-1090">Protector</w>
       <w lemma="of" pos="acp" xml:id="A53687-001-b-1100">of</w>
       <w lemma="the" pos="d" xml:id="A53687-001-b-1110">the</w>
       <w lemma="commonwealth" pos="n1" reg="Commonwealth" xml:id="A53687-001-b-1120">Comonwealth</w>
       <w lemma="of" pos="acp" xml:id="A53687-001-b-1130">of</w>
       <w lemma="England" pos="nn1" xml:id="A53687-001-b-1140">England</w>
       <w lemma="Scotland" pos="nn1" xml:id="A53687-001-b-1150">Scotland</w>
       <w lemma="and" pos="cc" xml:id="A53687-001-b-1160">and</w>
       <w lemma="Ireland" pos="nn1" xml:id="A53687-001-b-1170">Ireland</w>
       <w lemma="and" pos="cc" xml:id="A53687-001-b-1180">and</w>
       <w lemma="the" pos="d" xml:id="A53687-001-b-1190">the</w>
       <w lemma="dominion" pos="n2" xml:id="A53687-001-b-1200">dominions</w>
       <w lemma="thereof" pos="av" xml:id="A53687-001-b-1210">thereof</w>
       <pc xml:id="A53687-001-b-1220">,</pc>
       <w lemma="or" pos="cc" xml:id="A53687-001-b-1230">or</w>
       <w lemma="this" pos="d" xml:id="A53687-001-b-1240">this</w>
       <w lemma="city" pos="n1" xml:id="A53687-001-b-1250">City</w>
       <pc xml:id="A53687-001-b-1260">,</pc>
       <w lemma="might" pos="n1" xml:id="A53687-001-b-1270">might</w>
       <pc xml:id="A53687-001-b-1280">,</pc>
       <w lemma="or" pos="cc" xml:id="A53687-001-b-1290">or</w>
       <w lemma="may" pos="vmb" xml:id="A53687-001-b-1300">may</w>
       <w lemma="lose" pos="vvi" xml:id="A53687-001-b-1310">lose</w>
       <w lemma="their" pos="po" xml:id="A53687-001-b-1320">their</w>
       <w lemma="custom" pos="n2" reg="Customs" xml:id="A53687-001-b-1330">Customes</w>
       <w lemma="or" pos="cc" xml:id="A53687-001-b-1340">or</w>
       <w lemma="advantage" pos="n2" xml:id="A53687-001-b-1350">advantages</w>
       <pc unit="sentence" xml:id="A53687-001-b-1360">.</pc>
       <w lemma="you" pos="pn" xml:id="A53687-001-b-1370">Ye</w>
       <w lemma="shall" pos="vmb" xml:id="A53687-001-b-1380">shall</w>
       <w lemma="know" pos="vvi" xml:id="A53687-001-b-1390">know</w>
       <w lemma="no" pos="dx" xml:id="A53687-001-b-1400">no</w>
       <w lemma="foreigner" pos="n1" reg="Foreigner" xml:id="A53687-001-b-1410">Forreigner</w>
       <w lemma="to" pos="prt" xml:id="A53687-001-b-1420">to</w>
       <w lemma="buy" pos="vvi" xml:id="A53687-001-b-1430">buy</w>
       <w lemma="or" pos="cc" xml:id="A53687-001-b-1440">or</w>
       <w lemma="sell" pos="vvi" xml:id="A53687-001-b-1450">sell</w>
       <w lemma="any" pos="d" xml:id="A53687-001-b-1460">any</w>
       <w lemma="merchandise" pos="n1" reg="Merchandise" xml:id="A53687-001-b-1470">Marchandize</w>
       <pc xml:id="A53687-001-b-1480">,</pc>
       <w lemma="with" pos="acp" xml:id="A53687-001-b-1490">with</w>
       <w lemma="any" pos="d" xml:id="A53687-001-b-1500">any</w>
       <w lemma="other" pos="d" xml:id="A53687-001-b-1510">other</w>
       <w lemma="foreigner" pos="n1" reg="Foreigner" xml:id="A53687-001-b-1520">Forreigner</w>
       <w lemma="within" pos="acp" xml:id="A53687-001-b-1530">within</w>
       <w lemma="this" pos="d" xml:id="A53687-001-b-1540">this</w>
       <w lemma="city" pos="n1" xml:id="A53687-001-b-1550">City</w>
       <w lemma="or" pos="cc" xml:id="A53687-001-b-1560">or</w>
       <w lemma="franchise" pos="n1" xml:id="A53687-001-b-1570">Franchise</w>
       <w lemma="thereof" pos="av" xml:id="A53687-001-b-1580">thereof</w>
       <pc xml:id="A53687-001-b-1590">,</pc>
       <w lemma="but" pos="acp" xml:id="A53687-001-b-1600">but</w>
       <w lemma="you" pos="pn" xml:id="A53687-001-b-1610">ye</w>
       <w lemma="shall" pos="vmb" xml:id="A53687-001-b-1620">shall</w>
       <w lemma="warn" pos="vvi" reg="warn" xml:id="A53687-001-b-1630">warne</w>
       <w lemma="the" pos="d" xml:id="A53687-001-b-1640">the</w>
       <w lemma="chamberlain" pos="n1" xml:id="A53687-001-b-1650">Chamberlain</w>
       <w lemma="thereof" pos="av" xml:id="A53687-001-b-1660">thereof</w>
       <pc xml:id="A53687-001-b-1670">,</pc>
       <w lemma="or" pos="cc" xml:id="A53687-001-b-1680">or</w>
       <w lemma="some" pos="d" xml:id="A53687-001-b-1690">some</w>
       <w lemma="minister" pos="n1" xml:id="A53687-001-b-1700">Minister</w>
       <w lemma="of" pos="acp" xml:id="A53687-001-b-1710">of</w>
       <w lemma="the" pos="d" xml:id="A53687-001-b-1720">the</w>
       <w lemma="chamber" pos="n1" xml:id="A53687-001-b-1730">Chamber</w>
       <pc unit="sentence" xml:id="A53687-001-b-1740">.</pc>
       <w lemma="you" pos="pn" xml:id="A53687-001-b-1750">Ye</w>
       <w lemma="shall" pos="vmb" xml:id="A53687-001-b-1760">shall</w>
       <w lemma="implead" pos="vvi" xml:id="A53687-001-b-1770">implead</w>
       <w lemma="or" pos="cc" xml:id="A53687-001-b-1780">or</w>
       <w lemma="sue" pos="vvi" xml:id="A53687-001-b-1790">sue</w>
       <w lemma="no" pos="dx" xml:id="A53687-001-b-1800">no</w>
       <w lemma="freeman" pos="n1" reg="Freeman" xml:id="A53687-001-b-1810">Free-man</w>
       <w lemma="out" pos="av" xml:id="A53687-001-b-1820">out</w>
       <w lemma="of" pos="acp" xml:id="A53687-001-b-1830">of</w>
       <w lemma="this" pos="d" xml:id="A53687-001-b-1840">this</w>
       <w lemma="city" pos="n1" xml:id="A53687-001-b-1850">City</w>
       <pc xml:id="A53687-001-b-1860">,</pc>
       <w lemma="while" pos="cs" xml:id="A53687-001-b-1870">whiles</w>
       <w lemma="you" pos="pn" xml:id="A53687-001-b-1880">ye</w>
       <w lemma="may" pos="vmb" xml:id="A53687-001-b-1890">may</w>
       <w lemma="have" pos="vvi" xml:id="A53687-001-b-1900">have</w>
       <w lemma="right" pos="j" xml:id="A53687-001-b-1910">right</w>
       <w lemma="and" pos="cc" xml:id="A53687-001-b-1920">and</w>
       <w lemma="law" pos="n1" xml:id="A53687-001-b-1930">law</w>
       <w lemma="within" pos="acp" xml:id="A53687-001-b-1940">within</w>
       <w lemma="the" pos="d" xml:id="A53687-001-b-1950">the</w>
       <w lemma="same" pos="d" xml:id="A53687-001-b-1960">same</w>
       <w lemma="city" pos="n1" xml:id="A53687-001-b-1970">City</w>
       <pc unit="sentence" xml:id="A53687-001-b-1980">.</pc>
       <w lemma="you" pos="pn" xml:id="A53687-001-b-1990">Ye</w>
       <w lemma="shall" pos="vmb" xml:id="A53687-001-b-2000">shall</w>
       <w lemma="take" pos="vvi" xml:id="A53687-001-b-2010">take</w>
       <w lemma="none" pos="pix" xml:id="A53687-001-b-2020">none</w>
       <w lemma="apprentice" pos="n1" xml:id="A53687-001-b-2030">Apprentice</w>
       <pc xml:id="A53687-001-b-2040">,</pc>
       <w lemma="but" pos="acp" xml:id="A53687-001-b-2050">but</w>
       <w lemma="if" pos="cs" xml:id="A53687-001-b-2060">if</w>
       <w lemma="he" pos="pns" xml:id="A53687-001-b-2070">he</w>
       <w lemma="be" pos="vvb" xml:id="A53687-001-b-2080">be</w>
       <w lemma="freeborn" pos="j" reg="Freeborn" xml:id="A53687-001-b-2090">Free-born</w>
       <pc xml:id="A53687-001-b-2100">,</pc>
       <pc join="right" xml:id="A53687-001-b-2110">(</pc>
       <w lemma="that" pos="cs" xml:id="A53687-001-b-2120">that</w>
       <w lemma="be" pos="vvz" xml:id="A53687-001-b-2130">is</w>
       <w lemma="to" pos="prt" xml:id="A53687-001-b-2140">to</w>
       <w lemma="say" pos="vvi" xml:id="A53687-001-b-2150">say</w>
       <pc xml:id="A53687-001-b-2160">)</pc>
       <w lemma="no" pos="dx" xml:id="A53687-001-b-2170">no</w>
       <w lemma="bondmans" pos="ng1" reg="bondmans'" xml:id="A53687-001-b-2180">bondmans</w>
       <w lemma="son" pos="n1" xml:id="A53687-001-b-2190">son</w>
       <pc xml:id="A53687-001-b-2200">,</pc>
       <w lemma="nor" pos="ccx" xml:id="A53687-001-b-2210">nor</w>
       <w lemma="the" pos="d" xml:id="A53687-001-b-2220">the</w>
       <w lemma="child" pos="n1" xml:id="A53687-001-b-2230">child</w>
       <w lemma="of" pos="acp" xml:id="A53687-001-b-2240">of</w>
       <w lemma="any" pos="d" xml:id="A53687-001-b-2250">any</w>
       <w lemma="alien" pos="n1-j" xml:id="A53687-001-b-2260">Alien</w>
       <pc xml:id="A53687-001-b-2270">,</pc>
       <w lemma="and" pos="cc" xml:id="A53687-001-b-2280">and</w>
       <w lemma="for" pos="acp" xml:id="A53687-001-b-2290">for</w>
       <w lemma="no" pos="dx" xml:id="A53687-001-b-2300">no</w>
       <w lemma="less" pos="dc" reg="less" xml:id="A53687-001-b-2310">lesse</w>
       <w lemma="term" pos="n1" reg="term" xml:id="A53687-001-b-2320">tearm</w>
       <w lemma="than" pos="cs" reg="than" xml:id="A53687-001-b-2330">then</w>
       <w lemma="for" pos="acp" xml:id="A53687-001-b-2340">for</w>
       <w lemma="seven" pos="crd" xml:id="A53687-001-b-2350">Seven</w>
       <w lemma="year" pos="n2" xml:id="A53687-001-b-2360">years</w>
       <w lemma="without" pos="acp" xml:id="A53687-001-b-2370">without</w>
       <w lemma="fraud" pos="n1" xml:id="A53687-001-b-2380">fraud</w>
       <w lemma="or" pos="cc" xml:id="A53687-001-b-2390">or</w>
       <w lemma="deceit" pos="n1" xml:id="A53687-001-b-2400">deceit</w>
       <pc xml:id="A53687-001-b-2410">:</pc>
       <w lemma="and" pos="cc" xml:id="A53687-001-b-2420">and</w>
       <w lemma="within" pos="acp" xml:id="A53687-001-b-2430">within</w>
       <w lemma="the" pos="d" xml:id="A53687-001-b-2440">the</w>
       <w lemma="first" pos="ord" xml:id="A53687-001-b-2450">first</w>
       <w lemma="year" pos="n1" xml:id="A53687-001-b-2460">year</w>
       <w lemma="you" pos="pn" xml:id="A53687-001-b-2470">ye</w>
       <w lemma="shall" pos="vmb" xml:id="A53687-001-b-2480">shall</w>
       <w lemma="cause" pos="vvi" xml:id="A53687-001-b-2490">cause</w>
       <w lemma="he" pos="pno" xml:id="A53687-001-b-2500">him</w>
       <w lemma="to" pos="prt" xml:id="A53687-001-b-2510">to</w>
       <w lemma="be" pos="vvi" xml:id="A53687-001-b-2520">be</w>
       <w lemma="enrol" pos="vvn" reg="enroled" xml:id="A53687-001-b-2530">enrolled</w>
       <pc xml:id="A53687-001-b-2540">,</pc>
       <w lemma="or" pos="cc" xml:id="A53687-001-b-2550">or</w>
       <w lemma="else" pos="av" xml:id="A53687-001-b-2560">else</w>
       <w lemma="pay" pos="vvi" xml:id="A53687-001-b-2570">pay</w>
       <w lemma="such" pos="d" xml:id="A53687-001-b-2580">such</w>
       <w lemma="fine" pos="j" xml:id="A53687-001-b-2590">fine</w>
       <w lemma="as" pos="acp" xml:id="A53687-001-b-2600">as</w>
       <w lemma="shall" pos="vmb" xml:id="A53687-001-b-2610">shall</w>
       <w lemma="be" pos="vvi" xml:id="A53687-001-b-2620">be</w>
       <w lemma="reasonable" pos="av-j" xml:id="A53687-001-b-2630">reasonably</w>
       <w lemma="impose" pos="vvn" xml:id="A53687-001-b-2640">imposed</w>
       <w lemma="upon" pos="acp" xml:id="A53687-001-b-2650">upon</w>
       <w lemma="you" pos="pn" xml:id="A53687-001-b-2660">you</w>
       <w lemma="for" pos="acp" xml:id="A53687-001-b-2670">for</w>
       <w lemma="omit" pos="vvg" xml:id="A53687-001-b-2680">omitting</w>
       <w lemma="the" pos="d" xml:id="A53687-001-b-2690">the</w>
       <w lemma="same" pos="d" xml:id="A53687-001-b-2700">same</w>
       <pc unit="sentence" xml:id="A53687-001-b-2710">.</pc>
       <w lemma="and" pos="cc" xml:id="A53687-001-b-2720">And</w>
       <w lemma="after" pos="acp" xml:id="A53687-001-b-2730">after</w>
       <w lemma="his" pos="po" xml:id="A53687-001-b-2740">his</w>
       <w lemma="term" pos="n2" xml:id="A53687-001-b-2750">terms</w>
       <w lemma="end" pos="vvi" xml:id="A53687-001-b-2760">end</w>
       <pc xml:id="A53687-001-b-2770">,</pc>
       <w lemma="within" pos="acp" xml:id="A53687-001-b-2780">within</w>
       <w lemma="convenient" pos="j" xml:id="A53687-001-b-2790">convenient</w>
       <w lemma="time" pos="n1" xml:id="A53687-001-b-2800">time</w>
       <pc join="right" xml:id="A53687-001-b-2810">(</pc>
       <w lemma="be" pos="vvg" xml:id="A53687-001-b-2820">being</w>
       <w lemma="require" pos="vvn" xml:id="A53687-001-b-2830">required</w>
       <pc xml:id="A53687-001-b-2840">)</pc>
       <w lemma="you" pos="pn" xml:id="A53687-001-b-2850">ye</w>
       <w lemma="shall" pos="vmb" xml:id="A53687-001-b-2860">shall</w>
       <w lemma="make" pos="vvi" xml:id="A53687-001-b-2870">make</w>
       <w lemma="he" pos="pno" xml:id="A53687-001-b-2880">him</w>
       <w lemma="free" pos="j" xml:id="A53687-001-b-2890">Free</w>
       <w lemma="of" pos="acp" xml:id="A53687-001-b-2900">of</w>
       <w lemma="this" pos="d" xml:id="A53687-001-b-2910">this</w>
       <w lemma="city" pos="n1" xml:id="A53687-001-b-2920">City</w>
       <pc xml:id="A53687-001-b-2930">,</pc>
       <w lemma="if" pos="cs" xml:id="A53687-001-b-2940">if</w>
       <w lemma="he" pos="pns" xml:id="A53687-001-b-2950">he</w>
       <w lemma="have" pos="vvb" xml:id="A53687-001-b-2960">have</w>
       <w lemma="well" pos="av" xml:id="A53687-001-b-2970">well</w>
       <w lemma="and" pos="cc" xml:id="A53687-001-b-2980">and</w>
       <w lemma="true" pos="av-j" xml:id="A53687-001-b-2990">truly</w>
       <w lemma="serve" pos="vvn" xml:id="A53687-001-b-3000">served</w>
       <w lemma="you" pos="pn" xml:id="A53687-001-b-3010">you</w>
       <pc unit="sentence" xml:id="A53687-001-b-3020">.</pc>
       <w lemma="you" pos="pn" xml:id="A53687-001-b-3030">Ye</w>
       <w lemma="shall" pos="vmb" xml:id="A53687-001-b-3040">shall</w>
       <w lemma="also" pos="av" xml:id="A53687-001-b-3050">also</w>
       <w lemma="keep" pos="vvi" xml:id="A53687-001-b-3060">keep</w>
       <w lemma="the" pos="d" xml:id="A53687-001-b-3070">the</w>
       <w lemma="public" pos="j" reg="public" xml:id="A53687-001-b-3080">publicke</w>
       <w lemma="peace" pos="n1" xml:id="A53687-001-b-3090">Peace</w>
       <w lemma="in" pos="acp" xml:id="A53687-001-b-3100">in</w>
       <w lemma="your" pos="po" xml:id="A53687-001-b-3110">your</w>
       <w lemma="own" pos="d" xml:id="A53687-001-b-3120">own</w>
       <w lemma="person" pos="n1" xml:id="A53687-001-b-3130">person</w>
       <pc unit="sentence" xml:id="A53687-001-b-3140">.</pc>
       <w lemma="you" pos="pn" xml:id="A53687-001-b-3150">Ye</w>
       <w lemma="shall" pos="vmb" xml:id="A53687-001-b-3160">shall</w>
       <w lemma="know" pos="vvi" xml:id="A53687-001-b-3170">know</w>
       <w lemma="no" pos="dx" xml:id="A53687-001-b-3180">no</w>
       <w lemma="gather" pos="n2-vg" xml:id="A53687-001-b-3190">gatherings</w>
       <pc xml:id="A53687-001-b-3200">,</pc>
       <w lemma="nor" pos="ccx" xml:id="A53687-001-b-3210">nor</w>
       <w lemma="conspiraciess" pos="n1" xml:id="A53687-001-b-3220">conspiraciess</w>
       <w lemma="make" pos="vvd" xml:id="A53687-001-b-3230">made</w>
       <w lemma="against" pos="acp" xml:id="A53687-001-b-3240">against</w>
       <w lemma="the" pos="d" xml:id="A53687-001-b-3250">the</w>
       <w lemma="public" pos="j" reg="public" xml:id="A53687-001-b-3260">publicke</w>
       <w lemma="peace" pos="n1" xml:id="A53687-001-b-3270">Peace</w>
       <w lemma="but" pos="acp" xml:id="A53687-001-b-3280">but</w>
       <w lemma="you" pos="pn" xml:id="A53687-001-b-3290">ye</w>
       <w lemma="shall" pos="vmb" xml:id="A53687-001-b-3300">shall</w>
       <w lemma="warn" pos="vvi" xml:id="A53687-001-b-3310">warn</w>
       <w lemma="the" pos="d" xml:id="A53687-001-b-3320">the</w>
       <w lemma="mayor" pos="n1" reg="Mayor" xml:id="A53687-001-b-3330">Maior</w>
       <w lemma="thereof" pos="av" xml:id="A53687-001-b-3340">thereof</w>
       <pc xml:id="A53687-001-b-3350">,</pc>
       <w lemma="or" pos="cc" xml:id="A53687-001-b-3360">or</w>
       <w lemma="let" pos="vvb" xml:id="A53687-001-b-3370">let</w>
       <w lemma="it" pos="pn" xml:id="A53687-001-b-3380">it</w>
       <w lemma="to" pos="acp" xml:id="A53687-001-b-3390">to</w>
       <w lemma="your" pos="po" xml:id="A53687-001-b-3400">your</w>
       <w lemma="power" pos="n1" xml:id="A53687-001-b-3410">power</w>
       <pc unit="sentence" xml:id="A53687-001-b-3420">.</pc>
       <w lemma="all" pos="av-d" xml:id="A53687-001-b-3430">All</w>
       <w lemma="these" pos="d" xml:id="A53687-001-b-3440">these</w>
       <w lemma="point" pos="n2" reg="points" xml:id="A53687-001-b-3450">poynts</w>
       <w lemma="and" pos="cc" xml:id="A53687-001-b-3460">and</w>
       <w lemma="article" pos="n2" xml:id="A53687-001-b-3470">articles</w>
       <w lemma="you" pos="pn" xml:id="A53687-001-b-3480">ye</w>
       <w lemma="shall" pos="vmb" xml:id="A53687-001-b-3490">shall</w>
       <w lemma="well" pos="av" xml:id="A53687-001-b-3500">well</w>
       <w lemma="and" pos="cc" xml:id="A53687-001-b-3510">and</w>
       <w lemma="true" pos="av-j" xml:id="A53687-001-b-3520">truly</w>
       <w lemma="keep" pos="vvi" xml:id="A53687-001-b-3530">keep</w>
       <pc xml:id="A53687-001-b-3540">,</pc>
       <w lemma="according" pos="j" xml:id="A53687-001-b-3550">according</w>
       <w lemma="to" pos="acp" xml:id="A53687-001-b-3560">to</w>
       <w lemma="the" pos="d" xml:id="A53687-001-b-3570">the</w>
       <w lemma="law" pos="n2" xml:id="A53687-001-b-3580">laws</w>
       <w lemma="and" pos="cc" xml:id="A53687-001-b-3590">and</w>
       <w lemma="custom" pos="n2" xml:id="A53687-001-b-3600">customs</w>
       <w lemma="of" pos="acp" xml:id="A53687-001-b-3610">of</w>
       <w lemma="this" pos="d" xml:id="A53687-001-b-3620">this</w>
       <w lemma="city" pos="n1" xml:id="A53687-001-b-3630">City</w>
       <pc xml:id="A53687-001-b-3640">,</pc>
       <w lemma="to" pos="acp" xml:id="A53687-001-b-3650">to</w>
       <w lemma="your" pos="po" xml:id="A53687-001-b-3660">your</w>
       <w lemma="power" pos="n1" xml:id="A53687-001-b-3670">power</w>
       <pc unit="sentence" xml:id="A53687-001-b-3680">.</pc>
      </p>
     </div>
    </body>
   </text>
  </group>
 </text>
</TEI>
