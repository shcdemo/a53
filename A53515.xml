<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="A53515">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>Epilogue to Her Royal Highness, on her return from Scotland written by Mr. Otway.</title>
    <author>Otway, Thomas, 1652-1685.</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription A53515 of text R10445 in the <ref target="http;//estc.bl.uk">English Short Title Catalog</ref> (Wing O546). Textual changes  and metadata enrichments aim at making the                             text more  computationally tractable, easier to read, and suitable for network-based collaborative                              curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in                             a standardized format that preserves archaic forms ('loveth', 'seekest').                              Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 2 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2018</date>
    <idno type="DLPS">A53515</idno>
    <idno type="STC">Wing O546</idno>
    <idno type="STC">ESTC R10445</idno>
    <idno type="EEBO-CITATION">12590716</idno>
    <idno type="OCLC">ocm 12590716</idno>
    <idno type="VID">63906</idno>
    <idno type="PROQUESTGOID">2240988892</idno>
    <availability>
     <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 1, no. A53515)</note>
    <note>Transcribed from: (Early English Books Online ; image set 63906)</note>
    <note>Images scanned from microfilm: (Early English books, 1641-1700 ; 967:1)</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>Epilogue to Her Royal Highness, on her return from Scotland written by Mr. Otway.</title>
      <author>Otway, Thomas, 1652-1685.</author>
     </titleStmt>
     <extent>1 sheet ([1] p.)</extent>
     <publicationStmt>
      <publisher>Printed for Jacob Tonson ...,</publisher>
      <pubPlace>[London] :</pubPlace>
      <date>1682.</date>
     </publicationStmt>
     <notesStmt>
      <note>Reproduction of original in Huntington Library.</note>
      <note>Broadside.</note>
      <note>In verse.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>Mary, -- of Modena, Queen, consort of James II, King of England, 1658-1718.</term>
     <term>Broadsides -- England -- London -- 17th century.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:corpus>eebo</ep:corpus>
    <ep:title>Epilogue to Her Royal Highness, on her return from Scotland. Written by Mr. Otway.</ep:title>
    <ep:author>Otway, Thomas, </ep:author>
    <ep:publicationYear>1682</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>2</ep:pageCount>
    <ep:wordCount>275</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change><date>2002-12</date><label>TCP</label>
        Assigned for keying and markup
      </change>
   <change><date>2003-02</date><label>Apex CoVantage</label>
        Keyed and coded from ProQuest page images
      </change>
   <change><date>2003-03</date><label>Mona Logarbo</label>
        Sampled and proofread
      </change>
   <change><date>2003-03</date><label>Mona Logarbo</label>
        Text and markup reviewed and edited
      </change>
   <change><date>2003-04</date><label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="A53515-t">
  <body xml:id="A53515-e0">
   <div type="epilogue" xml:id="A53515-e10">
    <pb facs="tcp:63906:1" xml:id="A53515-001-a"/>
    <pb facs="tcp:63906:1" rend="simple:additions" xml:id="A53515-001-b"/>
    <head xml:id="A53515-e20">
     <w lemma="epilogue" pos="n1" xml:id="A53515-001-b-0010">EPILOGUE</w>
     <w lemma="to" pos="acp" xml:id="A53515-001-b-0020">TO</w>
     <w lemma="her" pos="po" xml:id="A53515-001-b-0030">Her</w>
     <w lemma="royal" pos="j" xml:id="A53515-001-b-0040">Royal</w>
     <w lemma="highness" pos="n1" xml:id="A53515-001-b-0050">Highness</w>
     <pc xml:id="A53515-001-b-0060">,</pc>
     <w lemma="on" pos="acp" xml:id="A53515-001-b-0070">On</w>
     <w lemma="her" pos="po" xml:id="A53515-001-b-0080">Her</w>
     <w lemma="return" pos="n1" xml:id="A53515-001-b-0090">RETURN</w>
     <w lemma="from" pos="acp" xml:id="A53515-001-b-0100">from</w>
     <w lemma="Scotland" pos="nn1" xml:id="A53515-001-b-0110">SCOTLAND</w>
     <pc unit="sentence" xml:id="A53515-001-b-0120">.</pc>
    </head>
    <byline xml:id="A53515-e30">
     <w lemma="write" pos="j-vn" xml:id="A53515-001-b-0130">Written</w>
     <w lemma="by" pos="acp" xml:id="A53515-001-b-0140">by</w>
     <w lemma="mr." pos="ab" xml:id="A53515-001-b-0150">Mr.</w>
     <w lemma="otway" pos="nn1" rend="hi" xml:id="A53515-001-b-0160">OTWAY</w>
     <pc unit="sentence" xml:id="A53515-001-b-0170">.</pc>
    </byline>
    <lg xml:id="A53515-e50">
     <l xml:id="A53515-e60">
      <w lemma="all" pos="d" xml:id="A53515-001-b-0180">ALL</w>
      <w lemma="you" pos="pn" xml:id="A53515-001-b-0190">you</w>
      <pc xml:id="A53515-001-b-0200">,</pc>
      <w lemma="who" pos="crq" xml:id="A53515-001-b-0210">who</w>
      <w lemma="this" pos="d" xml:id="A53515-001-b-0220">this</w>
      <w join="right" lemma="day" pos="n1" xml:id="A53515-001-b-0230">Day</w>
      <w join="left" lemma="be" pos="vvz" xml:id="A53515-001-b-0231">'s</w>
      <w lemma="jubilee" pos="n1" xml:id="A53515-001-b-0240">Jubilee</w>
      <w lemma="attend" pos="vvb" xml:id="A53515-001-b-0250">attend</w>
      <pc xml:id="A53515-001-b-0260">,</pc>
     </l>
     <l xml:id="A53515-e70">
      <w lemma="and" pos="cc" xml:id="A53515-001-b-0270">And</w>
      <w lemma="every" pos="d" xml:id="A53515-001-b-0280">every</w>
      <w lemma="loyal" pos="j" xml:id="A53515-001-b-0290">Loyal</w>
      <w lemma="muse" pos="n2" xml:id="A53515-001-b-0300">Muses</w>
      <w lemma="loyal" pos="j" xml:id="A53515-001-b-0310">Loyal</w>
      <w lemma="friend" pos="n1" xml:id="A53515-001-b-0320">Friend</w>
      <pc xml:id="A53515-001-b-0330">;</pc>
     </l>
     <l xml:id="A53515-e80">
      <w lemma="that" pos="cs" xml:id="A53515-001-b-0340">That</w>
      <w lemma="come" pos="vvb" xml:id="A53515-001-b-0350">come</w>
      <w lemma="to" pos="prt" xml:id="A53515-001-b-0360">to</w>
      <w lemma="treat" pos="vvi" xml:id="A53515-001-b-0370">treat</w>
      <w lemma="your" pos="po" xml:id="A53515-001-b-0380">your</w>
      <w lemma="long" pos="j-vg" xml:id="A53515-001-b-0390">longing</w>
      <w lemma="wish" pos="n2" xml:id="A53515-001-b-0400">wishes</w>
      <w lemma="here" pos="av" xml:id="A53515-001-b-0410">here</w>
      <pc xml:id="A53515-001-b-0420">,</pc>
     </l>
     <l xml:id="A53515-e90">
      <w lemma="turn" pos="vvb" xml:id="A53515-001-b-0430">Turn</w>
      <w lemma="your" pos="po" xml:id="A53515-001-b-0440">your</w>
      <w lemma="desire" pos="j-vg" xml:id="A53515-001-b-0450">desiring</w>
      <w lemma="eye" pos="n2" xml:id="A53515-001-b-0460">Eyes</w>
      <w lemma="and" pos="cc" xml:id="A53515-001-b-0470">and</w>
      <w lemma="feast" pos="vvi" xml:id="A53515-001-b-0480">feast</w>
      <w lemma="they" pos="pno" xml:id="A53515-001-b-0490">'em</w>
      <pc xml:id="A53515-001-b-0500">,</pc>
      <w lemma="there" pos="av" xml:id="A53515-001-b-0510">there</w>
      <pc unit="sentence" xml:id="A53515-001-b-0520">.</pc>
     </l>
     <l xml:id="A53515-e100">
      <w lemma="thus" pos="av" xml:id="A53515-001-b-0530">Thus</w>
      <w lemma="fall" pos="vvg" xml:id="A53515-001-b-0540">falling</w>
      <w lemma="on" pos="acp" xml:id="A53515-001-b-0550">on</w>
      <w lemma="your" pos="po" xml:id="A53515-001-b-0560">your</w>
      <w lemma="knee" pos="n2" xml:id="A53515-001-b-0570">Knees</w>
      <w lemma="with" pos="acp" xml:id="A53515-001-b-0580">with</w>
      <w lemma="i" pos="pno" xml:id="A53515-001-b-0590">me</w>
      <w lemma="implore" pos="vvi" xml:id="A53515-001-b-0600">implore</w>
      <pc xml:id="A53515-001-b-0610">,</pc>
     </l>
     <l xml:id="A53515-e110">
      <w lemma="may" pos="vmb" xml:id="A53515-001-b-0620">May</w>
      <w lemma="this" pos="d" xml:id="A53515-001-b-0630">this</w>
      <w lemma="poor" pos="j" xml:id="A53515-001-b-0640">poor</w>
      <w lemma="land" pos="n1" xml:id="A53515-001-b-0650">Land</w>
      <w lemma="never" pos="avx" xml:id="A53515-001-b-0660">ne'er</w>
      <w lemma="lose" pos="vvi" xml:id="A53515-001-b-0670">lose</w>
      <w lemma="that" pos="d" xml:id="A53515-001-b-0680">that</w>
      <w lemma="presence" pos="n1" xml:id="A53515-001-b-0690">Presence</w>
      <w lemma="more" pos="avc-d" xml:id="A53515-001-b-0700">more</w>
      <pc xml:id="A53515-001-b-0710">:</pc>
     </l>
     <l xml:id="A53515-e120">
      <w lemma="but" pos="acp" xml:id="A53515-001-b-0720">But</w>
      <w lemma="if" pos="cs" xml:id="A53515-001-b-0730">if</w>
      <w lemma="there" pos="av" xml:id="A53515-001-b-0740">there</w>
      <w lemma="any" pos="d" xml:id="A53515-001-b-0750">any</w>
      <w lemma="in" pos="acp" xml:id="A53515-001-b-0760">in</w>
      <w lemma="this" pos="d" xml:id="A53515-001-b-0770">this</w>
      <w lemma="circle" pos="n1" xml:id="A53515-001-b-0780">Circle</w>
      <w lemma="be" pos="vvi" xml:id="A53515-001-b-0790">be</w>
      <pc xml:id="A53515-001-b-0800">,</pc>
     </l>
     <l xml:id="A53515-e130">
      <w lemma="that" pos="cs" xml:id="A53515-001-b-0810">That</w>
      <w lemma="come" pos="vvb" xml:id="A53515-001-b-0820">come</w>
      <w lemma="so" pos="av" xml:id="A53515-001-b-0830">so</w>
      <w lemma="curse" pos="vvn" reg="cursed" xml:id="A53515-001-b-0840">curst</w>
      <w lemma="to" pos="prt" xml:id="A53515-001-b-0850">to</w>
      <w lemma="envy" pos="vvi" xml:id="A53515-001-b-0860">envy</w>
      <w lemma="what" pos="crq" xml:id="A53515-001-b-0870">what</w>
      <w lemma="they" pos="pns" xml:id="A53515-001-b-0880">they</w>
      <w lemma="see" pos="vvb" xml:id="A53515-001-b-0890">see</w>
      <pc xml:id="A53515-001-b-0900">:</pc>
     </l>
     <l xml:id="A53515-e140">
      <w lemma="from" pos="acp" xml:id="A53515-001-b-0910">From</w>
      <w lemma="the" pos="d" xml:id="A53515-001-b-0920">the</w>
      <w lemma="vain" pos="j" xml:id="A53515-001-b-0930">vain</w>
      <w lemma="fool" pos="n1" xml:id="A53515-001-b-0940">Fool</w>
      <w lemma="that" pos="cs" xml:id="A53515-001-b-0950">that</w>
      <w lemma="will" pos="vmd" xml:id="A53515-001-b-0960">would</w>
      <w lemma="be" pos="vvi" xml:id="A53515-001-b-0970">be</w>
      <w lemma="great" pos="j" xml:id="A53515-001-b-0980">great</w>
      <w lemma="too" pos="av" xml:id="A53515-001-b-0990">too</w>
      <w lemma="soon" pos="av" xml:id="A53515-001-b-1000">soon</w>
      <pc xml:id="A53515-001-b-1010">,</pc>
     </l>
     <l xml:id="A53515-e150">
      <w lemma="to" pos="prt" xml:id="A53515-001-b-1020">To</w>
      <w lemma="the" pos="d" xml:id="A53515-001-b-1030">the</w>
      <w lemma="dull" pos="j" xml:id="A53515-001-b-1040">dull</w>
      <w lemma="knave" pos="n1" xml:id="A53515-001-b-1050">Knave</w>
      <w lemma="that" pos="cs" xml:id="A53515-001-b-1060">that</w>
      <w lemma="write" pos="vvd" xml:id="A53515-001-b-1070">writ</w>
      <w lemma="the" pos="d" xml:id="A53515-001-b-1080">the</w>
      <w lemma="last" pos="ord" xml:id="A53515-001-b-1090">last</w>
      <w lemma="lampoon" pos="n1" xml:id="A53515-001-b-1100">Lampoon</w>
      <pc unit="sentence" xml:id="A53515-001-b-1110">!</pc>
     </l>
     <l xml:id="A53515-e160">
      <w lemma="let" pos="vvb" xml:id="A53515-001-b-1120">Let</w>
      <w lemma="such" pos="d" xml:id="A53515-001-b-1130">such</w>
      <pc xml:id="A53515-001-b-1140">,</pc>
      <w lemma="as" pos="acp" xml:id="A53515-001-b-1150">as</w>
      <w lemma="victim" pos="n2" xml:id="A53515-001-b-1160">Victims</w>
      <w lemma="to" pos="acp" xml:id="A53515-001-b-1170">to</w>
      <w lemma="that" pos="d" xml:id="A53515-001-b-1180">that</w>
      <w join="right" lemma="beauty" pos="n1" reg="beauty" xml:id="A53515-001-b-1190">Beautie</w>
      <w join="left" lemma="be" pos="vvz" xml:id="A53515-001-b-1191">'s</w>
      <w lemma="fame" pos="n1" xml:id="A53515-001-b-1200">Fame</w>
      <pc xml:id="A53515-001-b-1210">,</pc>
     </l>
     <l xml:id="A53515-e170">
      <w lemma="hang" pos="vvb" xml:id="A53515-001-b-1220">Hang</w>
      <w lemma="their" pos="po" xml:id="A53515-001-b-1230">their</w>
      <w lemma="vile" pos="j" xml:id="A53515-001-b-1240">vile</w>
      <w lemma="blast" pos="j-vn" xml:id="A53515-001-b-1250">blasted</w>
      <w lemma="head" pos="n2" xml:id="A53515-001-b-1260">Heads</w>
      <pc xml:id="A53515-001-b-1270">,</pc>
      <w lemma="and" pos="cc" xml:id="A53515-001-b-1280">and</w>
      <w lemma="dye" pos="vvb" reg="Die" xml:id="A53515-001-b-1290">Dye</w>
      <w lemma="with" pos="acp" xml:id="A53515-001-b-1300">with</w>
      <w lemma="shame" pos="n1" xml:id="A53515-001-b-1310">shame</w>
      <pc xml:id="A53515-001-b-1320">,</pc>
     </l>
     <l xml:id="A53515-e180">
      <w lemma="our" pos="po" xml:id="A53515-001-b-1330">Our</w>
      <w lemma="mighty" pos="j" xml:id="A53515-001-b-1340">mighty</w>
      <w lemma="blessing" pos="n1" xml:id="A53515-001-b-1350">Blessing</w>
      <w lemma="be" pos="vvz" xml:id="A53515-001-b-1360">is</w>
      <w lemma="at" pos="acp" xml:id="A53515-001-b-1370">at</w>
      <w lemma="last" pos="ord" xml:id="A53515-001-b-1380">last</w>
      <w lemma="return" pos="vvn" reg="returned" xml:id="A53515-001-b-1390">return'd</w>
      <pc xml:id="A53515-001-b-1400">,</pc>
     </l>
     <l xml:id="A53515-e190">
      <w lemma="the" pos="d" xml:id="A53515-001-b-1410">The</w>
      <w lemma="joy" pos="n1" xml:id="A53515-001-b-1420">joy</w>
      <w lemma="arrive" pos="vvn" reg="arrived" xml:id="A53515-001-b-1430">arriv'd</w>
      <w lemma="for" pos="acp" xml:id="A53515-001-b-1440">for</w>
      <w lemma="which" pos="crq" xml:id="A53515-001-b-1450">which</w>
      <w lemma="so" pos="av" xml:id="A53515-001-b-1460">so</w>
      <w lemma="long" pos="av-j" xml:id="A53515-001-b-1470">long</w>
      <w lemma="we" pos="pns" xml:id="A53515-001-b-1480">we</w>
      <w lemma="mourn" pos="vvd" reg="mourned" xml:id="A53515-001-b-1490">mourn'd</w>
      <pc xml:id="A53515-001-b-1500">:</pc>
     </l>
     <l xml:id="A53515-e200">
      <w lemma="from" pos="acp" xml:id="A53515-001-b-1510">From</w>
      <w lemma="who" pos="crq" xml:id="A53515-001-b-1520">whom</w>
      <w lemma="our" pos="po" xml:id="A53515-001-b-1530">our</w>
      <w lemma="present" pos="j" xml:id="A53515-001-b-1540">present</w>
      <w lemma="peace" pos="n1" xml:id="A53515-001-b-1550">peace</w>
      <w lemma="we" pos="pns" xml:id="A53515-001-b-1560">we</w>
      <w lemma="'" pos="sy" xml:id="A53515-001-b-1570">'</w>
      <w lemma="expect" pos="vvi" xml:id="A53515-001-b-1580">expect</w>
      <w lemma="increase" pos="vvn" reg="increased" xml:id="A53515-001-b-1590">increas't</w>
      <pc xml:id="A53515-001-b-1600">,</pc>
     </l>
     <l xml:id="A53515-e210">
      <w lemma="and" pos="cc" xml:id="A53515-001-b-1610">And</w>
      <w lemma="all" pos="d" xml:id="A53515-001-b-1620">all</w>
      <w lemma="our" pos="po" xml:id="A53515-001-b-1630">our</w>
      <w lemma="future" pos="j" xml:id="A53515-001-b-1640">future</w>
      <w lemma="generation" pos="n2" xml:id="A53515-001-b-1650">Generations</w>
      <w lemma="blessed" pos="vvn" reg="blessed" xml:id="A53515-001-b-1660">blest</w>
      <pc xml:id="A53515-001-b-1670">:</pc>
     </l>
     <l xml:id="A53515-e220">
      <w lemma="time" pos="n1" xml:id="A53515-001-b-1680">Time</w>
      <w lemma="have" pos="vvb" xml:id="A53515-001-b-1690">have</w>
      <w lemma="a" pos="d" xml:id="A53515-001-b-1700">a</w>
      <w lemma="care" pos="n1" xml:id="A53515-001-b-1710">Care</w>
      <pc xml:id="A53515-001-b-1720">:</pc>
      <w lemma="bring" pos="vvb" xml:id="A53515-001-b-1730">bring</w>
      <w lemma="safe" pos="j" xml:id="A53515-001-b-1740">safe</w>
      <w lemma="the" pos="d" xml:id="A53515-001-b-1750">the</w>
      <w lemma="hour" pos="n1" xml:id="A53515-001-b-1760">hour</w>
      <w lemma="of" pos="acp" xml:id="A53515-001-b-1770">of</w>
      <w lemma="joy" pos="n1" xml:id="A53515-001-b-1780">joy</w>
     </l>
     <l xml:id="A53515-e230">
      <w lemma="when" pos="crq" xml:id="A53515-001-b-1790">When</w>
      <w lemma="some" pos="d" xml:id="A53515-001-b-1800">some</w>
      <w lemma="bless" pos="j-vn" reg="blessed" xml:id="A53515-001-b-1810">blest</w>
      <w lemma="tongue" pos="n1" xml:id="A53515-001-b-1820">Tongue</w>
      <w lemma="proclaim" pos="vvz" xml:id="A53515-001-b-1830">proclaims</w>
      <w lemma="a" pos="d" xml:id="A53515-001-b-1840">a</w>
      <w lemma="royal" pos="j" xml:id="A53515-001-b-1850">Royal</w>
      <w lemma="boy" pos="n1" xml:id="A53515-001-b-1860">Boy</w>
      <pc xml:id="A53515-001-b-1870">:</pc>
     </l>
     <l xml:id="A53515-e240">
      <w lemma="and" pos="cc" xml:id="A53515-001-b-1880">And</w>
      <w lemma="when" pos="crq" xml:id="A53515-001-b-1890">when</w>
      <w join="right" lemma="it" pos="pn" xml:id="A53515-001-b-1900">'t</w>
      <w join="left" lemma="be" pos="vvz" xml:id="A53515-001-b-1901">is</w>
      <w lemma="bear" pos="vvn" xml:id="A53515-001-b-1910">born</w>
      <pc xml:id="A53515-001-b-1920">,</pc>
      <w lemma="let" pos="vvb" xml:id="A53515-001-b-1930">let</w>
      <w lemma="nature" pos="ng1" xml:id="A53515-001-b-1940">Nature's</w>
      <w lemma="hand" pos="n1" xml:id="A53515-001-b-1950">hand</w>
      <w lemma="be" pos="vvi" xml:id="A53515-001-b-1960">be</w>
      <w lemma="strong" pos="j" xml:id="A53515-001-b-1970">strong</w>
      <pc xml:id="A53515-001-b-1980">;</pc>
     </l>
     <l xml:id="A53515-e250">
      <w lemma="bless" pos="vvb" xml:id="A53515-001-b-1990">Bless</w>
      <w lemma="he" pos="pno" xml:id="A53515-001-b-2000">him</w>
      <w lemma="with" pos="acp" xml:id="A53515-001-b-2010">with</w>
      <w lemma="day" pos="n2" xml:id="A53515-001-b-2020">days</w>
      <w lemma="of" pos="acp" xml:id="A53515-001-b-2030">of</w>
      <w lemma="strength" pos="n1" xml:id="A53515-001-b-2040">strength</w>
      <w lemma="and" pos="cc" xml:id="A53515-001-b-2050">and</w>
      <w lemma="make" pos="vvi" xml:id="A53515-001-b-2060">make</w>
      <w lemma="they" pos="pno" xml:id="A53515-001-b-2070">'em</w>
      <w lemma="long" pos="av-j" xml:id="A53515-001-b-2080">long</w>
      <pc xml:id="A53515-001-b-2090">;</pc>
     </l>
     <l xml:id="A53515-e260">
      <w lemma="till" pos="acp" xml:id="A53515-001-b-2100">Till</w>
      <w lemma="charge" pos="vvd" reg="charged" xml:id="A53515-001-b-2110">charg'd</w>
      <w lemma="with" pos="acp" xml:id="A53515-001-b-2120">with</w>
      <w lemma="honour" pos="n2" reg="honours" xml:id="A53515-001-b-2130">honors</w>
      <w lemma="we" pos="pns" xml:id="A53515-001-b-2140">we</w>
      <w lemma="behold" pos="vvb" xml:id="A53515-001-b-2150">behold</w>
      <w lemma="he" pos="pno" xml:id="A53515-001-b-2160">him</w>
      <w lemma="stand" pos="vvi" xml:id="A53515-001-b-2170">stand</w>
      <pc xml:id="A53515-001-b-2180">,</pc>
     </l>
     <l xml:id="A53515-e270">
      <w lemma="three" pos="crd" xml:id="A53515-001-b-2190">Three</w>
      <w lemma="kingdom" pos="n2" xml:id="A53515-001-b-2200">Kingdoms</w>
      <w lemma="banner" pos="n2" xml:id="A53515-001-b-2210">Banners</w>
      <w lemma="wait" pos="vvg" xml:id="A53515-001-b-2220">waiting</w>
      <w lemma="his" pos="po" xml:id="A53515-001-b-2230">his</w>
      <w lemma="command" pos="n1" xml:id="A53515-001-b-2240">Command</w>
      <pc xml:id="A53515-001-b-2250">,</pc>
     </l>
     <l xml:id="A53515-e280">
      <w lemma="his" pos="po" xml:id="A53515-001-b-2260">His</w>
      <w lemma="father" pos="ng1" xml:id="A53515-001-b-2270">Father's</w>
      <w lemma="conquer" pos="j-vg" xml:id="A53515-001-b-2280">Conquering</w>
      <w lemma="sword" pos="n1" xml:id="A53515-001-b-2290">Sword</w>
      <w lemma="within" pos="acp" xml:id="A53515-001-b-2300">within</w>
      <w lemma="his" pos="po" xml:id="A53515-001-b-2310">his</w>
      <w lemma="hand" pos="n1" xml:id="A53515-001-b-2320">Hand</w>
      <pc xml:id="A53515-001-b-2330">:</pc>
     </l>
     <l xml:id="A53515-e290">
      <w lemma="then" pos="av" xml:id="A53515-001-b-2340">Then</w>
      <w lemma="the" pos="d" xml:id="A53515-001-b-2350">th'</w>
      <w lemma="english" pos="jnn" xml:id="A53515-001-b-2360">English</w>
      <w lemma="lion" pos="n2" xml:id="A53515-001-b-2370">Lions</w>
      <w lemma="in" pos="acp" xml:id="A53515-001-b-2380">in</w>
      <w lemma="the" pos="d" xml:id="A53515-001-b-2390">the</w>
      <w lemma="air" pos="n1" xml:id="A53515-001-b-2400">Air</w>
      <w lemma="advance" pos="vvi" xml:id="A53515-001-b-2410">advance</w>
      <pc xml:id="A53515-001-b-2420">,</pc>
     </l>
     <l xml:id="A53515-e300">
      <w lemma="and" pos="cc" xml:id="A53515-001-b-2430">And</w>
      <w lemma="with" pos="acp" xml:id="A53515-001-b-2440">with</w>
      <w lemma="they" pos="pno" xml:id="A53515-001-b-2450">them</w>
      <w lemma="roar" pos="vvg" xml:id="A53515-001-b-2460">roaring</w>
      <w lemma="music" pos="n1" reg="Music" xml:id="A53515-001-b-2470">Musick</w>
      <w lemma="to" pos="acp" xml:id="A53515-001-b-2480">to</w>
      <w lemma="the" pos="d" xml:id="A53515-001-b-2490">the</w>
      <w lemma="dance" pos="n1" xml:id="A53515-001-b-2500">Dance</w>
      <pc xml:id="A53515-001-b-2510">,</pc>
     </l>
     <l xml:id="A53515-e310">
      <w lemma="carry" pos="vvb" xml:id="A53515-001-b-2520">Carry</w>
      <w lemma="a" pos="d" xml:id="A53515-001-b-2530">a</w>
      <foreign xml:id="A53515-e320" xml:lang="lat">
       <w lemma="quo" pos="fla" xml:id="A53515-001-b-2540">Quo</w>
       <w lemma="warranto" pos="fla" xml:id="A53515-001-b-2550">Warranto</w>
      </foreign>
      <w lemma="into" pos="acp" xml:id="A53515-001-b-2560">into</w>
      <w lemma="France" pos="nn1" rend="hi" xml:id="A53515-001-b-2570">France</w>
      <pc unit="sentence" xml:id="A53515-001-b-2580">.</pc>
     </l>
    </lg>
   </div>
  </body>
  <back xml:id="A53515-e340">
   <div type="colophon" xml:id="A53515-e350">
    <p xml:id="A53515-e360">
     <w lemma="print" pos="j-vn" xml:id="A53515-001-b-2590">Printed</w>
     <w lemma="for" pos="acp" xml:id="A53515-001-b-2600">for</w>
     <hi xml:id="A53515-e370">
      <w lemma="Jacob" pos="nn1" xml:id="A53515-001-b-2610">Jacob</w>
      <w lemma="tonson" pos="nn1" xml:id="A53515-001-b-2620">Tonson</w>
     </hi>
     <pc rend="follows-hi" xml:id="A53515-001-b-2630">,</pc>
     <w lemma="at" pos="acp" xml:id="A53515-001-b-2640">at</w>
     <w lemma="the" pos="d" xml:id="A53515-001-b-2650">the</w>
     <w lemma="judge" pos="ng1" rend="hi-apo-plain" xml:id="A53515-001-b-2660">Judge's</w>
     <w lemma="head" pos="n1" rend="hi" xml:id="A53515-001-b-2680">Head</w>
     <w lemma="in" pos="acp" xml:id="A53515-001-b-2690">in</w>
     <w lemma="Chancery-lane" pos="nn1" rend="hi" xml:id="A53515-001-b-2700">Chancery-lane</w>
     <pc xml:id="A53515-001-b-2710">,</pc>
     <w lemma="1682." pos="crd" xml:id="A53515-001-b-2720">1682.</w>
     <pc unit="sentence" xml:id="A53515-001-b-2730"/>
    </p>
   </div>
  </back>
 </text>
</TEI>
